
DLP Analytics:

Analytics installation Role: analytics-installation

Following task will be performed by the playbook

1. check analytics already installed or not
2. Download analytics installable file from Artifactory
3. install the analytics application
4. install and configure snmp and chrony 